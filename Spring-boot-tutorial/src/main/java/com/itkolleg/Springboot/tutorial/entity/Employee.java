package com.itkolleg.Springboot.tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Employee
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long employeeId;

    @NotBlank(message = "Please add a First Name")
    private String firstname;

    @NotBlank(message = "Please add a Last Name")
    private String lastname;

    @NotBlank(message = "Please add an E-Mail")
    private String email;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "department_code", referencedColumnName = "departmentCode")
    private Department department;
}
