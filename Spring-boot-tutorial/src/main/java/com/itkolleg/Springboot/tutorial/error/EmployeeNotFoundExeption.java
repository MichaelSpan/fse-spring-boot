package com.itkolleg.Springboot.tutorial.error;

import com.itkolleg.Springboot.tutorial.entity.Employee;

public class EmployeeNotFoundExeption extends Exception
{
    public EmployeeNotFoundExeption() {
        super();
    }

    public EmployeeNotFoundExeption(String message) {
        super(message);
    }

    public EmployeeNotFoundExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public EmployeeNotFoundExeption(Throwable cause) {
        super(cause);
    }

    protected EmployeeNotFoundExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
