package com.itkolleg.Springboot.tutorial.service;

import com.itkolleg.Springboot.tutorial.entity.Employee;
import com.itkolleg.Springboot.tutorial.error.EmployeeNotFoundExeption;

import java.util.List;

public interface EmployeeService
{
    public Employee saveEmployee(Employee employee);

    public List<Employee> fetchEmployeeList();

    Employee fetchEmployeeById(Long employeeId) throws EmployeeNotFoundExeption;

    public void deleteEmployeeById(Long employeeId);

    public Employee updateEmployee(Long employeeId, Employee employee);

    public List<Employee> fetchEmployeeListByDepartmentCode(String departmentCode);
}
