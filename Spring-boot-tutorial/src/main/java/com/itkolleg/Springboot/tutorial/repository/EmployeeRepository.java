package com.itkolleg.Springboot.tutorial.repository;

import com.itkolleg.Springboot.tutorial.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>
{
    @Query(value = "SELECT * FROM Employee WHERE departmentCode = ?1", nativeQuery = true)
    List<Employee> fetchEmployeeListByDepartmentCode(String departmentCode);
}
