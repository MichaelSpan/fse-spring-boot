package com.itkolleg.Springboot.tutorial.service;

import com.itkolleg.Springboot.tutorial.entity.Employee;
import com.itkolleg.Springboot.tutorial.error.EmployeeNotFoundExeption;
import com.itkolleg.Springboot.tutorial.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService
{
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee saveEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public List<Employee> fetchEmployeeList() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee fetchEmployeeById(Long employeeId) throws EmployeeNotFoundExeption {
        Optional<Employee> employee = employeeRepository.findById(employeeId);

        if (!employee.isPresent()) {
            throw new EmployeeNotFoundExeption("Employee not available");
        }

        return employee.get();
    }

    @Override
    public void deleteEmployeeById(Long employeeId) {
        employeeRepository.deleteById(employeeId);
    }

    @Override
    public Employee updateEmployee(Long employeeId, Employee employee) {
        Employee empDB = employeeRepository.findById(employeeId).get();

        if (Objects.nonNull(employee.getFirstname()) && !"".equalsIgnoreCase(employee.getFirstname())) {
            empDB.setFirstname(employee.getFirstname());
        }

        if (Objects.nonNull(employee.getLastname()) && !"".equalsIgnoreCase(employee.getLastname())) {
            empDB.setLastname(employee.getLastname());
        }

        if (Objects.nonNull(employee.getEmail()) && !"".equalsIgnoreCase(employee.getEmail())) {
            empDB.setEmail(employee.getEmail());
        }
        return employeeRepository.save(empDB);
    }

    @Override
    public List<Employee> fetchEmployeeListByDepartmentCode(String departmentCode)
    {
        return employeeRepository.fetchEmployeeListByDepartmentCode(departmentCode);
    }
}
