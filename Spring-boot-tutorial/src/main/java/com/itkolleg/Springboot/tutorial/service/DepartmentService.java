package com.itkolleg.Springboot.tutorial.service;

import com.itkolleg.Springboot.tutorial.entity.Department;
import com.itkolleg.Springboot.tutorial.error.DepartmentNotFoundExeption;

import java.util.List;

public interface DepartmentService {
    public Department saveDepartment(Department department);

    public List<Department> fetchDepartmentList();

    Department fetchDepartmentById(Long departmentId) throws DepartmentNotFoundExeption;

    public void deleteDepartmentById(Long departmentId);

    public Department updateDepartment(Long departmentId, Department department);

    public Department fetchDepartmentByName(String departmentName);
}
