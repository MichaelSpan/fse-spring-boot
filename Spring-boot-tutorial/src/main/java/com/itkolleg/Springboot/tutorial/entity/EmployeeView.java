package com.itkolleg.Springboot.tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeView
{
    private Long employeeId;
    private String firstname;
    private String lastname;
    private String email;
    private String departmentCode;
}
