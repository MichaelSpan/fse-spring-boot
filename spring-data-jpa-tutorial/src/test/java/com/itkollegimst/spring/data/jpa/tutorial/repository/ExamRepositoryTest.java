package com.itkollegimst.spring.data.jpa.tutorial.repository;

import com.itkollegimst.spring.data.jpa.tutorial.entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@SpringBootTest
class ExamRepositoryTest
{
    @Autowired
    private ExamRepository examRepository;

    @Test
    public void printExams()
    {
        List<Exam> exams = examRepository.findAll();

        System.out.println("exams -> " + exams);
    }

    @Test
    public void saveExamWithSupervisorAndCourse() throws ParseException {
        Teacher supervisor = Teacher.builder().firstName("Manuel").lastName("Forsthuber").build();

        Course course = Course.builder().title("test").teacher(supervisor).credit(10).build();

        Date date = new SimpleDateFormat( "yyyyMMdd" ).parse( "20211222" );

        Exam exam = Exam.builder().supervisor(supervisor).course(course).date(date).build();

        examRepository.save(exam);
    }

    @Test
    public void printExamByExamId()
    {
       Optional<Exam> exam = examRepository.findById(4L);

       ExamView examView = ExamView.builder()
               .examId(exam.get().getExamId())
               .courseId(exam.get().getCourse().getCourseId())
               .supervisorId(exam.get().getSupervisor().getTeacherId())
               .date(exam.get().getDate())
               .build();


        System.out.println("exam: " + examView);
    }

    /*@Test
    public void printExamByCourseId()
    {
        List<Exam> exams = examRepository.findByCourseId(9L);

        System.out.println("Exams: " + exams);
    }*/
}