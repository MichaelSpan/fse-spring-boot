package com.itkollegimst.spring.data.jpa.tutorial.repository;

import com.itkollegimst.spring.data.jpa.tutorial.entity.Guardian;
import com.itkollegimst.spring.data.jpa.tutorial.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class StudentRepositoryTest
{
    @Autowired
    private StudentRepository studentRepository;

    @Test
    public void saveStudent()
    {
        Student student = Student.builder().emailId("micspan@tsn.at").firstName("Michael").lastName("Span")
                //.guardianName("Helmut").guardianEmail("gasthof.schatz@cnh.at").guardianMobile("123")
        .build();

        studentRepository.save(student);
    }

    @Test
    public void saveStudentWithGuardian()
    {
        Guardian guardian = Guardian.builder().name("Norbert").email("norbertpurner@gmx.at").mobile("1234567890").build();

        Student student = Student.builder().firstName("Adrian").lastName("Purner").emailId("adrianpurner@gmail.com").guardian(guardian).build();

        studentRepository.save(student);
    }

    @Test
    public void printStudentByFirstName()
    {
        List<Student> students = studentRepository.findByFirstName("Michael");

        System.out.println("Students: " + students);
    }

    @Test
    public void printStudentByFirstNameContaining()
    {
        List<Student> students = studentRepository.findByFirstNameContaining("i");

        System.out.println("Students: " + students);
    }

    @Test
    public void printAllStudents()
    {
        List<Student> studentList = studentRepository.findAll();

        System.out.println("Student List: " + studentList);
    }

    @Test
    public void printStudentBasedOnGuardianName()
    {
        List<Student> students = studentRepository.findByGuardianName("Norbert");

        System.out.println("STUDENTS= " + students);
    }

    @Test
    public void printGetStudentByEmailAddress()
    {
        Student student = studentRepository.getStudentByEmailAddress("micspan@tsn.at");

        System.out.println("STudENt: " + student);
    }

    @Test
    public void printGetStudentFirstNameByEmailAddress()
    {
        String student = studentRepository.getStudentFirstNameByEmailAddress("micspan@tsn.at");

        System.out.println("STudENt: " + student);
    }

    @Test
    public void printStudentGetStudentByEmailAddressNative()
    {
        Student student = studentRepository.getStudentByEmailAddressNative("adrianpurner@gmail.com");

        System.out.println("stUDenT: " + student);
    }

    @Test
    public void printGetStudentByEmailAddressNativeNamedParam()
    {
        Student student = studentRepository.getStudentByEmailAddressNativeNamedParam("micspan@tsn.at");

        System.out.println("THE STUDENT: " + student);
    }

    @Test
    public void updateStudentNameByEmailId()
    {
        studentRepository.updateStudentNameByEmailId("Manuel", "micspan@tsn.at");
    }
}