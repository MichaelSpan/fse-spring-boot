package com.itkollegimst.spring.data.jpa.tutorial.repository;

import com.itkollegimst.spring.data.jpa.tutorial.entity.Exam;
import com.itkollegimst.spring.data.jpa.tutorial.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Repository
public interface ExamRepository extends JpaRepository<Exam, Long>
{
    //List<Exam> findByExamId(Long examId);

    /*List<Exam> findByCourseId(Long courseId);

    @Modifying
    @Transactional
    @Query(value = "update tbl_exam set date = ?1 where exam_id = ?2", nativeQuery = true)
    int updateExamDateByExamId(Date date, Long examId);*/
}
