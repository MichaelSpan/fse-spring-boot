package com.itkollegimst.spring.data.jpa.tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExamView {
    private Long examId;
    private Date date;
    private Long supervisorId;
    private Long courseId;
}
